# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
import datetime
from odoo.exceptions import ValidationError

class hotel(models.Model):
    _name = 'hotel.hotel'
    name = fields.Char()
    city = fields.Many2one('hotel.cities')
    direccion = fields.Char()
    phone = fields.Char()
    photosgallery = fields.One2many('hotel.photos','hotel')
    photo_principal = fields.Binary(compute='seleccionarFoto',store=True)
    roomslist = fields.One2many('hotel.rooms','hoteles')
    stars = fields.Selection([('1','⭐'),('2','⭐⭐'),('3','⭐⭐⭐'),('4','⭐⭐⭐⭐'),('5','⭐⭐⭐⭐⭐')],readonly=True)
    serviceslist = fields.Many2many('hotel.services')
    valorations = fields.One2many('hotel.valorations','hotel')
    # Si no pose Selection, mostra el número d'estrelles
    clientStars = fields.Selection([('0','Este Hotel aún no tiene comentarios, quieres ser el primero?'),('1','😡'),('2','😞😞'),('3','🙂🙂🙂'),('4','😃😃😃😃'),('5','😍😍😍😍😍')],compute='_calcularMitja', store=True)
    bookinsListFuture = fields.Many2many('hotel.bookins',compute='_show_bookings_future',store=False)
    bookinsListPresent = fields.Many2many('hotel.bookins',compute='_show_bookings_present',store=False)
    bookinsListPast = fields.Many2many('hotel.bookins',compute='_show_bookings_past',store=False)

    @api.depends()
    def _show_bookings_future(self):
        for s in self:
            fecha = fields.Datetime.now()
            s.bookinsListFuture = s.env['hotel.bookins'].search([('room.hoteles.id', '=', s.id),('diaEntrada','>', fecha)]).ids

    @api.depends()
    def _show_bookings_present(self):
        for s in self:
            fecha = fields.Datetime.now()
            s.bookinsListPresent = s.env['hotel.bookins'].search([('room.hoteles.id', '=', s.id),('diaEntrada','<=', fecha),('diaSalida','>=', fecha)]).ids

    @api.depends()
    def _show_bookings_past(self):
        for s in self:
            fecha = fields.Datetime.now()
            s.bookinsListPast = s.env['hotel.bookins'].search([('room.hoteles.id', '=', s.id),('diaSalida','<', fecha)]).ids

# METODE PER A CARREGAR LA PRIMERA FOTO DEL ARRAY COM A PRINCIPAL
    @api.depends('photosgallery')
    def seleccionarFoto(self):
        for record in self:
            if len(record.photosgallery) > 0: # Comprobem que la longitud del array on tenim les fotos siga major que 0
                record.photo_principal=record.photosgallery[0].photo
            else: # En el cas de que no carreguem fotos al array, mostrem per consola per a saber-ho

            # ************  PREGUNTAR A JOSE !!!!! ***************
            # Si reiniciem tot el hotel i el posem sense datos, al iniciar el hotel i carregar tots els demos es mostra esta linea pero carrega les fotos dels hotels de forma correcta, recordar que el orden al manifest no es el correcte, pero si es carreguen les fotos
            # abans del hotel, dona error de External ID

                print("Vaya, parece que este hotel no tiene fotos, prueba a ejecutar de nuevo")

# METODE PER A CALCULAR LA MITJA DE VALORACIONS
    @api.depends('valorations')
    def _calcularMitja(self):
        #Fem un for per a tots els hotels
        for s in self:

            if len(s.valorations) > 0: # Si té una o mes valoracions
                i=0
                suma=0
                resultado=0

                # Fem un for per a cada valoracio de cada hotel
                for v in s.valorations:
                    suma=int(suma)+int(v.clientStars)
                    i=i+1
                resultado=suma/i
                s.clientStars=str(int(resultado)) # Aclaració!!! Es com fer un set en java on hi ha que fer un casting
            else: # No hi han valoracions, mostrem el valor per defecte
                s.clientStars='0'

class cities(models.Model):
    _name = 'hotel.cities'
    name = fields.Char()
    descripcio = fields.Char()
    ubicacio = fields.Char()
    pais = fields.Many2one('res.country')
    hotel = fields.One2many('hotel.hotel', 'city')

class rooms(models.Model):
    _name = 'hotel.rooms'
    name = fields.Char()
    llits = fields.Char()
    fotos = fields.Many2many("hotel.photos_rooms")
    preus = fields.Char()
    descripcio = fields.Char()
    hoteles = fields.Many2one('hotel.hotel')

class photos(models.Model):
    _name = 'hotel.photos'
    name = fields.Char()
    hotel = fields.Many2one('hotel.hotel')
    photo = fields.Binary()
    photo_small = fields.Binary(compute='_get_resized_image',store=True)

    #Redimensionem imatge
    @api.depends('photo')
    def _get_resized_image(self):
        for s in self:
            data = tools.image_get_resized_images(s.photo)
            s.photo_small = data['image_medium']

class photos_rooms(models.Model):
    _name = 'hotel.photos_rooms'
    name = fields.Char()
    photo_rooms = fields.Binary()

class services(models.Model):
    _name = 'hotel.services'
    name = fields.Char()
    photo_service = fields.Binary()

class bookins(models.Model):
    _name = 'hotel.bookins'
    name = fields.Char()
    client = fields.Many2one('res.partner')
    # Servix per a carregar de una forma automàtica a quin hotel perteneix una habitació
    hotel = fields.Many2one('hotel.hotel', related='room.hoteles',store=False,readonly=True)
    room = fields.Many2one('hotel.rooms')
    # Servix per a carregar de forma automàtica a quina ciutat perteneix un hotel
    ciutat = fields.Many2one('hotel.cities', related='room.hoteles.city',store=False,readonly=True)
    hotelBookin =fields.Many2one('hotel.hotel','hotel')
    diaEntrada = fields.Date()
    diaSalida = fields.Date()
    ventas = fields.Many2one('sale.order.line', compute='_venta_no_duplicada', store=False)

    #Exemple molt facil de restar dies: https://www.w3resource.com/python-exercises/python-basic-exercise-14.php
    # Pàgina top de fetxes (oficial de python): https://www.pythonprogramming.in/how-to-calculate-the-time-difference-between-two-datetime-objects.html

    # ***** IMPORTANT!!!! *****
    # L'ordre dels if ha de ser aquest, ja que si comprovem en el if que estiga el diaEntrada, no es pot cambiar el diaSalida, sempre es recalcularà de forma automàtica

    @api.onchange('diaEntrada','diaSalida')
    def _calcularDia(self):
        for record in self:
            if record.diaEntrada and record.diaSalida: # Comprobem que estiguen les dos dates escrites

                # En el que cas de que estiguen les dos dates escrites, comprobem que la data d'eixida siga més gran que la d'entrada

                formato='%Y-%m-%d'
                diaEntrada=datetime.datetime.strptime(str(record.diaEntrada),formato)
                diaSalida=datetime.datetime.strptime(str(record.diaSalida),formato)
                if diaSalida < diaEntrada: # Si la data d'eixida es menor que la d'entrada, aumenten de forma automàtica dos dies la d'eixida respecte a la d'ntrada
                    record.diaSalida = diaEntrada + datetime.timedelta(days=2)
                    return { # En este punt hi ha que fer un return warning, ja que si gastem un raise, mostra l'error pero no actualitza la data d'eixida
                        'warning': {
                            'title': "ERROR",
                            'message': "No puedes seleccionar un Día de Salida anterior al Día de Entrada",
                        }
                    }

            elif record.diaEntrada: # Cuant es posa la data d'entrada, de forma automática sumem dos dies
                formato = '%Y-%m-%d'
                data = datetime.datetime.strptime(str(record.diaEntrada), formato)
                record.diaSalida = data + datetime.timedelta(days=2)

# Metode per a comprobar si es pot reservar una habitació
    @api.constrains('diaEntrada', 'diaSalida')
    def _comprobar_reserva(self):
        for s in self:
            # Comprobem el id, que el dia d'entrada siga menor o igual que el d'eixida, que el dia d'eixida siga major o igual que el d'entrada i que la habitació siga igual, si no posen habitació, no es pot reservar cap altra habitació en tot el hotel en la mateixa fetxa
            resultado = self.search_count([('id', '!=', s.id), ('diaEntrada','<=', s.diaSalida), ('diaSalida', '>=', s.diaEntrada), ('room.id','=',s.room.id)]) # Habitacion diferente

            # Si el resultat es 1 o major, mostrem error, ja que eixa habitacio, està reservada el mateix dia
            if resultado > 0:
                raise ValidationError("La reserva con nombre: " + self.name + " no se puede realizar ya que ya existe otra reserva para la misma habitación con la misma fecha")

    @api.depends()
    def _venta_no_duplicada(self):
        for s in self:
            resultado = s.env['sale.order.line'].search([('listaBookings.id', '=', s.id)])

            if len(resultado) > 0:
                s.ventas = resultado[0].id

    # Mètode per a generar ventes desde una reserva
    @api.multi
    def crear_vendes(self):
        venta = self.env['sale.order'].create({'partner_id':self.client.id})
        product = self.env.ref('hotel.producto_reserva')
        self.env['sale.order.line'].create({'product_id':product.id, 'name': self.name, 'order_id':venta.id, 'listaBookings':self.id})

class valorations(models.Model):
    _name = 'hotel.valorations'
    client = fields.Many2one("res.partner")
    hotel=fields.Many2one('hotel.hotel','hotel')
    descripcio = fields.Char()
    clientStars = fields.Selection([('1','😡'),('2','😞😞'),('3','🙂🙂🙂'),('4','😃😃😃😃'),('5','😍😍😍😍😍')])

class cliente(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    listaValorations = fields.One2many('hotel.valorations','client')
    listaBookings = fields.One2many('hotel.bookins', 'client')
    listaBookingPagadas = fields.Many2many('hotel.bookins', compute='_bookings_Pagadas', store=False)
    listaBookingNoPagadas = fields.Many2many('hotel.bookins', compute='_bookings_No_Pagadas', store=False)

    def _bookings_Pagadas(self):
        for s in self:
            # el filtered lambda actua com a filtro en un if
            s.listaBookingPagadas = s.listaBookings.filtered(lambda s: len(s.ventas) > 0)

    def _bookings_No_Pagadas(self):
        for s in self:
            # el filtered lambda actua com a filtro en un if
            s.listaBookingNoPagadas = s.listaBookings.filtered(lambda s: len(s.ventas) == 0)

    @api.multi
    def pagar_reserva(self):
        venta = self.env['sale.order'].create({'partner_id':self.id})
        product = self.env.ref('hotel.producto_reserva')

        for s in self.listaBookingNoPagadas:
            self.env['sale.order.line'].create({'product_id':product.id, 'name': s.name, 'order_id':venta.id, 'listaBookings':s.id})

class sales(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'
    listaBookings = fields.Many2one('hotel.bookins')
    room = fields.Many2one('hotel.rooms', related='listaBookings.room',store=False,readonly=True)
    client = fields.Many2one('res.partner', related='listaBookings.client',store=False,readonly=True)
    hotel = fields.Many2one('hotel.hotel', related='listaBookings.hotel',store=False,readonly=True)
    diaEntrar = fields.Date(related='listaBookings.diaEntrada',store=False,readonly=True)
    diaSalir = fields.Date(related='listaBookings.diaSalida',store=False,readonly=True)

    # Métode per a comprobar si una reserva ja té una venta
    @api.constrains('listaBookings')
    def _comprobar_sales(self):
        for s in self:
            #if len(self.listaBookings) > 0:
            resultado = self.search_count([('listaBookings.id', '=', s.listaBookings.id)])

            if resultado > 1:
                raise ValidationError("La venta ya tiene una reserva")

class w_bookins(models.TransientModel):
    _name = "hotel.w_bookins"
    name = fields.Char()

class w_client(models.TransientModel):
    _name = "hotel.w_client"
    name = fields.Char()

class pagar_alguna_reserva_wizard(models.TransientModel):
    _name='pagar_alguna_reserva_wizard'

    # Retorne el client actual
    def _cliente_actual(self):
        return self.env['res.partner'].browse(self._context.get('active_id'))

    # Retorne factures pendents sense pagar
    def _facturas_pendientes(self):
        return self.env['res.partner'].browse(self._context.get('active_id')).listaBookingNoPagadas

    cli = fields.Many2one('res.partner', default=_cliente_actual)
    reserves_pendents_client = fields.Many2many('hotel.bookins',default=_facturas_pendientes)
    name=fields.Char(name="Nombre de la reserva" , related='reserves_pendents_client.name')
    #print(name)

    @api.multi
    def pagar(self):
        # Creem venta i producte nova
        venta = self.env['sale.order'].create({'partner_id':self.cli.id})
        product = self.env.ref('hotel.producto_reserva')
        #print(product)

        for reserva in self.reserves_pendents_client:
            #print("estic asi dins, provant")
            self.env['sale.order.line'].create({'product_id':product.id, 'name': reserva.name, 'order_id':venta.id, 'listaBookings':reserva.id})

class wizard(models.TransientModel):
        _name= 'hotel.bookins_wizard'
        nombre = fields.Char()
        # Retorne el client actual
        def _cliente_actual(self):
            return self.env['res.partner'].browse(self._context.get('active_id'))

        cli = fields.Many2one('res.partner', default=_cliente_actual,required=True)

        # Paisos
        # ATENCIO!!!!!
        # Posible problema, revisar en cas de que falle esta linea!!!
        # Asi faig many2many per a mostrar tots, i despres al many2one li pase la tabla per a mostrar nomes els camps que jo vullc
        paises = fields.Many2many('res.country', default=lambda r: r.env['hotel.cities'].search([]).mapped('pais').ids)
        pais = fields.Many2one('res.country')

        # Ciutats
        ciudades = fields.Many2many('hotel.cities')
        ciudad = fields.Many2one("hotel.cities",required=True)

        # Selection
        puntuacion = fields.Selection([('1','😡'),('2','😞😞'),('3','🙂🙂🙂'),('4','😃😃😃😃'),('5','😍😍😍😍😍')])
        stars = fields.Selection([('1','⭐'),('2','⭐⭐'),('3','⭐⭐⭐'),('4','⭐⭐⭐⭐'),('5','⭐⭐⭐⭐⭐')])
        beds = fields.Selection([('1', '1'), ('2', '2'), ('3', '3')])

        #Servicis
        serviceslist = fields.Many2many('hotel.services')

        #Fetxes
        diaEntrada = fields.Date(required=True)
        diaSalida = fields.Date(required=True)

        # Hotels
        hoteles = fields.Many2many("hotel.hotel",required=True)
        hotel = fields.Many2one("hotel.hotel")

        # Habitacions
        rooms = fields.Many2many('hotel.rooms')
        room = fields.Many2one("hotel.rooms")

        # Llits
        beds = fields.Selection([('1','1'), ('2','2'), ('3','3')])

        #room = fields.Many2one('hotel.rooms')

        #Cree els estats que va a tindre la selecció per a poder posar visible les opcions que per defecte están ocultes en la VISTA
        state = fields.Selection([
		('city', "City Selection"),
		('details', "Details Selection"),
		('hotel', "Hotel Selection"),
        ('beds', "Beds Selection"),
        ('room', "Room Selection"),
        ('book', "Book It!"),],
		default='city')

        #Cuant cambia el pais
        @api.onchange('pais')
        def pais_seleccionado(self):
            self.ciudades = self.env['hotel.cities'].search([('pais.id','=',self.pais.id)])

        #Cuant cambia la ciutat
        @api.onchange('ciudad')
        def ciudad_seleccionada(self):
            if len(self.ciudad) > 0:
                #Busque hotels d'eixa ciutat
                self.hoteles = self.env['hotel.hotel'].search([('city.id','=',self.ciudad.id)])
                #print(self.hoteles)
                #Afegisc al array que la ciutat siga la seleccionada
                self.state="details"

        #Cuant cambia la puntuacio
        @api.onchange('puntuacion')
        def mostrar_hoteles3(self):
            if (self.puntuacion):
                print("askdjhasdjkahjkdasd")
                self.hoteles = self.env['hotel.hotel'].search([('clientStars','=',self.puntuacion), ('city.id','=',self.ciudad.id)])

        #Cuant cambia Stars
        @api.onchange('stars')
        def stars_seleccionada(self):
            if (self.stars):
                #Busque i actualitze els hotels amb l'opció seleccionada
                self.hoteles = self.env['hotel.hotel'].search([('stars','=',self.stars), ('city.id','=',self.ciudad.id)])

        # Cuant cambia services
        @api.onchange('serviceslist')
        def servicio_seleccionado(self):
            if len(self.serviceslist) > 0:
                #Busque i actualitze els hotels amb l'opció seleccionada
                self.hoteles = self.env['hotel.hotel'].search([('serviceslist','=',self.serviceslist), ('city.id','=',self.ciudad.id), ('stars','=',self.stars)])

        # Cuant cambia hotel, actualitze les habitacions a les d'eixe hotel
        @api.onchange('hotel')
        def mostrar_habitaciones(self):
            if (self.hotel):
                self.rooms = self.env['hotel.rooms'].search([('hoteles.id','=',self.hotel.id)])
                self.state="beds"

        #Cuant cambia Bed active botó de habitacio
        @api.onchange('beds')
        def beds_seleccionados(self):
            if (self.beds):
                #Busque i actualitze les habitacions amb l'opció seleccionada
                self.rooms = self.env['hotel.rooms'].search([('llits','=',self.beds),('hoteles.id','=',self.hotel.id)])
                self.state="room"

        # Cuant cambie la habitació, active el botó de reservar
        @api.onchange('room')
        def room_seleccionados(self):
            if (self.room):
                self.state="book"

        #Mètode per a reservar en el wizard
        def reservar(self):
            reserva = self.env['hotel.bookins'].create({'name':self.nombre,'client':self.cli.id,'room':self.room.id, 'diaEntrada': self.diaEntrada, 'diaSalida':self.diaSalida})
