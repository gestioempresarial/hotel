#!/bin/bash
echo '<?xml version="1.0" encoding="utf-8"?>
	<odoo>
		<data>'
cont=0;
hoteles=("balnearioArchena" "hotelMontsant" "hotelEspanya" "hotelMadrid")

	while [ $cont -lt 400 ] ; do
	hotel=$(( RANDOM % 4 ))
	llits=$((( RANDOM % 3 ) + 1 ))
	preu=$((( RANDOM % 500 ) + 100))
		echo '
		<record model="hotel.rooms" id="room'$cont'">
			<field name="hoteles" ref="hotel.'${hoteles[$hotel]}'"></field>
			<field name="name">Room '$cont'</field>
			<field name="llits">'$llits'</field>
			<field name="preus">'$preu'</field>
			<field name="descripcio">Habitacion perfecta para relajarse</field>
			<field name="fotos" eval="[(6,0,[ref('"'hotel.photo_room_$hotel'"')] )]" />
		</record>
		'
	cont=$(( $cont + 1 ))
	done
echo '
	</data>
</odoo>
'
